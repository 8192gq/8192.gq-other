__author__ = 'Riley Flynn (nint8835)'
import sys
import urllib2
import json
import getpass

args = sys.argv

ip_delete = False

if args[2] == "lookup" and len(args)>=4:
    ip_delete = True

    print "Looking up IP for link given..."
    ip_resp = json.load(urllib2.urlopen("http://" + args[1] + "/api/Anon/link/" + args[3] + "/"))
    print "    IP recieved."
    print "    IP: " + ip_resp["IP"]
    ip = ip_resp["IP"]

if args[2] == "ip" and len(args)>=4:
    ip_delete = True
    ip = args[3]



if args.count("admin") >= 1:
    print "Links will be deleted as admin"
    key = getpass.getpass("Admin key: ")
    delete = "/api/" + key + "/admin/delete_key/"

else:
    print "Links will be deleted as a normal user"
    key = getpass.getpass("API key: ")
    delete = "/api/" + key + "/delete_key/"

if len(args)<2:
    sys.exit(0)

print "Retrieving list of links from server..."
print ip_delete
if ip_delete:
    response = urllib2.urlopen("http://" + args[1] + "/api/Anon/links_ip/" + ip)

else:
    response = urllib2.urlopen("http://" + args[1] + "/api/Anon/links")
data = json.load(response)
print "Recieved list of " + str(len(data)) + " link(s) from the server."

for index, resp_link in enumerate(data):
    print str(index + 1) + ": " + resp_link["ID"]
    print "    URL: " + resp_link["URL"]
    print "    IP:  " + resp_link["IP"]

if raw_input("Are you sure you wish to delete these " + str(len(data)) + " link(s) [y/n]? ").lower() == "y":
    print "Beginning deletion..."
    succeeded = 0
    for index, resp_link in enumerate(data):
        print "Deleting link " + str(index + 1) + "/" + str(len(data)) + ": " + resp_link["ID"] + " (" + resp_link["URL"] + ")"
        delete_resp = json.load(urllib2.urlopen("http://" + args[1] + delete + resp_link["ID"] + "/"))
        if delete_resp["status"] != "Deleted":
            print "    Deletion failed."
        else:
            print "    Link deleted."
            succeeded += 1

    print "Deletion completed. " + str(succeeded) + "/" + str(len(data)) + " links were successfully deleted."
